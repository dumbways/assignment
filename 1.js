`use strict`

const { replaceWord } = require('./helper')

let regency = replaceWord('purwokerto', 'o', 'a')
console.log('Replace character "o" to "a" from string "purwokerto"', regency)