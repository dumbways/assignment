`use strict`

const helper = {
    replaceWord(string, searchValue, replaceValue) {
        let res
        if(typeof string == 'string') {
            let processedString = []
            for (const index in string) {
                let word = string[index]
                processedString.push(word == searchValue ? replaceValue : word)
            }
            res = { status: true, data: processedString.join('') }
        } else {
            res = { status: false, message: 'Invalid input of string' }
        }
        return res
    },
    countHandshake(people) {
        let res = 0
        for(let first = 1; first < people; first++) {
            for(let second = first + 1; second <= people; second++) {
                res++
            }
        }
        /** You can use this code for testing */
        // res = people * (people - 1) / 2
        return res
    },
    betweenDates(start, end) {
        let res
        if(typeof start == 'string' && typeof end == 'string') {
            console.log(`Get Date Between ${start} & ${end}`)
            start = new Date(start)
            end = new Date(end)
            if(start <= end) {
                let dates = []
                do {
                    dates.push(`${start.getFullYear()}-${start.getMonth() + 1}-${start.getDate()}`)
                    start.setDate(start.getDate() + 1)
                } while(start <= end)
                res = { status: 1, data: dates.join(', ') }
            } else {
                res = { status: 0, message: 'Invalid input, Start Date must lower or equal than End Date' }
            }
        } else {
            res = { status: 0, message: 'Invalid input' }
        }
        return res
    },
    randomString(count = 1, length = 32) {
        let results = []
        for (let i = 0; i < count; i++) {
            let result = '', chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            for (let i = length; i > 0; --i) { 
                result += chars[Math.round(Math.random() * (chars.length - 1))];
            }
            results.push(result)
        }
        return results;
    },
    printSomething(number) {
        let res
        if(typeof number == 'number') {
            if(number % 2 == 1) {
                let plus = ' + ', equals = ' = ', middle = parseInt(number / 2) + 1
                for(let y = 1; y <= number; y++) {
                    let horizontal = ''
                    for(let x = 1; x <= number; x++) {
                        if(y == 1 || y == number || x == middle) {
                            horizontal += plus
                        } else {
                            horizontal += equals
                        }
                    }
                    /** Print Here */
                    console.log(horizontal)
                }
            } else {
                res = { status: 0, message: 'Input must be odd number' }
            }
        } else {
            res = { status: 0, message: 'Invalid input of number' }
        }
        return res
    }
}

module.exports = helper